<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        
        <script type="text/javascript" src="javascript/less-1.6.1.min.js"></script>
        <link rel="stylesheet/less" type="text/css" href="less/index.less"/>
    </head>
    <body>
        <?php
        // put your code here
        ?>
        
        <form class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend>FORM ALUMNOS</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">MARCA</label>  
  <div class="col-md-4">
  <input id="textinput" name="textinput" type="text" placeholder="MARCA" class="form-control input-md">
  <span class="help-block">MARCA</span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="MODELO">MODELO</label>  
  <div class="col-md-4">
  <input id="MODELO" name="MODELO" type="text" placeholder="MODELO" class="form-control input-md">
  <span class="help-block">MODELO</span>  
  </div>
</div>

<!-- Multiple Radios -->
<div class="form-group">
  <label class="col-md-4 control-label" for="RAM">RAM</label>
  <div class="col-md-4">
  <div class="radio">
    <label for="RAM-0">
      <input type="radio" name="RAM" id="RAM-0" value="1" checked="checked">
      1 GB
    </label>
	</div>
  <div class="radio">
    <label for="RAM-1">
      <input type="radio" name="RAM" id="RAM-1" value="2">
      2 GB
    </label>
	</div>
  </div>
</div>

<!-- Multiple Radios -->
<div class="form-group">
  <label class="col-md-4 control-label" for="">PROCESADOR</label>
  <div class="col-md-4">
  <div class="radio">
    <label for="-0">
      <input type="radio" name="" id="-0" value="1" checked="checked">
      QUAD CORE
    </label>
	</div>
  <div class="radio">
    <label for="-1">
      <input type="radio" name="" id="-1" value="2">
      OCTA CORE
    </label>
	</div>
  </div>
</div>

</fieldset>
</form>

        
        
        
    </body>
</html>
